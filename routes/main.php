<?php

Router::get('a.name', '/', [
    'use' => 'DefaultController@show',
]);
Router::post('a.post', '/', [
    'use' => 'DefaultController@handlePost',
]);