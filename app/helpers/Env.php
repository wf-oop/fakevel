<?php

class Env
{
    private $envData = null;
    private static $instance = null;

    /**
     * Make the construct method private to make sure we can not create a new instance
     */
    private function __construct()
    {
    }

    /**
     * Make the clone method private to make sure we can not create a new instance
     */
    private function __clone()
    {
    }

    /**
     * Returns the singleton instance
     * @return Env
     */
    public static function getInstance()
    {
        if(!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Load the data from the env file. If the env file is not found, throw an exception
     *
     * @throws Exception
     * @return void
     */
    public static function load()
    {
        $instance = self::getInstance();
        $envFile = parse_ini_file('../.env');

        if($envFile === false) {
            throw new Exception('Can\'t find an env file!');
        } else {
            $instance->envData = $envFile;
        }
    }

    /**
     * Read the value of a specific key from the env file
     *
     * @param $key
     * @return mixed
     */
    public static function read($key) {
        $instance = self::getInstance();

        if(isset($instance->envData[$key])) {
            return $instance->envData[$key];
        } else {
            return null;
        }
    }
}