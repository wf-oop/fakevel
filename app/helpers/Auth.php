<?php

class Auth
{
    /**
     * Make the construct method private to make sure we can not create a new instance
     */
    private function __construct()
    {
    }

    /**
     * Get the current user data from the session
     *
     * @return null
     */
    public static function getUserData()
    {
        if(isset($_SESSION['USER_DATA'])) {
            return $_SESSION['USER_DATA'];
        } else {
            return null;
        }
    }

    /**
     * Set new user data in the session
     *
     * @param User|null $user
     */
    public static function setUserData($user)
    {
        if($user === null) {
            unset($_SESSION['USER_DATA']);
        } else {
            $userData = new stdClass();

            $userData->id = $user->id;
            $userData->role = $user->role;
            $userData->username = $user->username;
            $userData->privilegeIds = static::getPrivilegeIdsByRole($userData->role);

            $_SESSION['USER_DATA'] = $userData;
        }
    }

    /**
     * Hash the password of an user
     *
     * @param String $password
     * @return bool|string
     */
    public static function hashPassword(String $password)
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }

    /**
     * Check if the password that is given equals the password from the user
     *
     * @param String $givenPassword
     * @param User $user
     * @return bool
     */
    public static function verifyPassword(String $givenPassword, User $user)
    {
        return password_verify($givenPassword, $user->password);
    }

    /**
     * Make the clone method private to make sure we can not create a new instance
     */
    private function __clone()
    {
    }
}