<?php

class DefaultController
{
    public function show()
    {
        return View::create('home');
    }

    public function handlePost()
    {
        return View::create('post', ['postData' => $_POST]);
    }
}