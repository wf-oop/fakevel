<?php

class DB
{
    private static $instance = null;

    private $pdo = null;

    /**
     * Make the construct method private to make sure we can not create a new instance
     */
    private function __construct()
    {
        $this->pdo = new PDO('mysql:host=' . env::read('DB_HOST') . ';dbname=' . env::read('DB_NAME'), env::read('DB_USER'), env::read('DB_PASS'));
        $this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
    }

    /**
     * Make the clone method private to make sure we can not create a new instance
     */
    private function __clone()
    {
    }

    /**
     * Easy method to get the pdo connection
     *
     * @return null|PDO
     */
    public static function getConnection()
    {
        return self::getInstance()->pdo;
    }

    /**
     * Returns the singleton instance
     * @return DB
     */
    public static function getInstance()
    {
        if(!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }
}