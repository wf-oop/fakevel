<?php

class IsLoggedIn
{
    /**
     * Run the middleware.
     * If the user is logged in, go further, if not, redirect to clients
     *
     * @param Closure $next
     * @return bool|mixed
     */
    public function run(Closure $next)
    {
        $user = Auth::getUserData();
        if($user) {
            return $next();
        } else {
            return Router::redirect('login.clients');
        }
    }
}