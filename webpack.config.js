const path = require('path');
const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const extractSCSS = new ExtractTextPlugin("../css/[name].css");

module.exports = {
    entry: {
        main: './resources',
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'web') + '/js'
    },
    plugins: [
        /*new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        }),*/
        extractSCSS
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                use: []
            },
            {
                test: /\.scss/i,
                use: extractSCSS.extract(['css-loader', 'sass-loader'])
            },
            {
                test: /.ttf$|.eot$|.woff2?$/,
                use: [
                    {
                        loader: 'file-loader?name=../fonts/[name].[ext]'
                    }
                ]
            }

            // Loaders for other file types can go here
        ]
    }
};