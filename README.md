# README #

This README would normally document whatever steps are necessary to get your application up and running.

### How do I get set up? ###

* copy env.example to .env
* edit .env file for your database

## How to use webpack ##
* install NPM/Node.js
* go to the project directory in the terminal/CLI

### For windows ###
* run `npm install`
* run `npm i -g webpack`
* run `webpack -w` to let webpack run every time there is a change in the scss/js
* run `webpack` to compile css/js once
* run `webpack -p` to compile css/js once for production

### For Unix-like ###
* run `npm install`
* run `npm run watch` to let webpack run every time there is a change in the scss/js
* run `npm run compile` to compile css/js once
* run `npm run production` to compile css/js once for production